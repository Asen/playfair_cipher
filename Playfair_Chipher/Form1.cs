﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Security;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Playfair_Chipher
{
    public partial class Main : Form
    {

        private Playfair cipher = new Playfair();
        private String pickedFileName = null;
        private String password = null;


        public Main()
        {
            InitializeComponent();         
        }


        private void openfileBtn_Click(object sender, EventArgs e)
        {
            if (cipherFileDialog.ShowDialog() == DialogResult.OK)
            {
                fileTextbox.Text = cipherFileDialog.FileName;
                pickedFileName = cipherFileDialog.FileName;
            }
        }


        private void keyTextbox_TextChanged(object sender, EventArgs e)
        {
            password = keyTextbox.Text;
        }


        // Метод проверяет входные данные по нажатию на 
        // кнопку Шифровки/Дешифровки
        private Boolean isInputValid()
        {
            bool isFilePicked = pickedFileName != null && pickedFileName.Length > 0;
            bool isFileExists = File.Exists(pickedFileName);
            bool isPwdSpecified = password != null && password.Length > 1;

            return isFilePicked && isFileExists && isPwdSpecified;
        }


        // Сообщение об ошибке в случае некорректного ввода
        private void ReportError()
        {
            MessageBox.Show(
                "Is file exists? Is password long enough?",
                "Invalid Input",
                MessageBoxButtons.OK,
                MessageBoxIcon.Warning
                );
        }

        
        // Шифровка
        private void encryptBtn_Click(object sender, EventArgs e)
        {
            if (isInputValid())
            {
                byte[] file_bytes = File.ReadAllBytes(pickedFileName);
                byte[] encrypted = cipher.Encrypt(file_bytes, password);

                String newFileName = GetNewFilename(pickedFileName, "encrypted");
                File.WriteAllBytes(newFileName, encrypted);
                MessageBox.Show("Done.\n\n"+String.Format("({0})", newFileName));
            }
            else
                ReportError();
        }


        // Дешифровка
        private void decryptBtn_Click(object sender, EventArgs e)
        {
            if (isInputValid())
            {
                byte[] file_bytes = File.ReadAllBytes(pickedFileName);
                byte[] decrypted = cipher.Decrypt(file_bytes, password);

                String newFileName = GetNewFilename(pickedFileName, "decrypted");
                File.WriteAllBytes(newFileName, decrypted);
                MessageBox.Show("Done.\n\n" + String.Format("({0})", newFileName));
            }
            else
                ReportError();
        }


        // Зашифрованные/Дешифрованные данные пишутся в файл с 
        // новым именем с суффиксом-сигнализатором
        private String GetNewFilename(String oldName, String suffix)
        {
            String path = Path.GetDirectoryName(oldName);
            String name = Path.GetFileNameWithoutExtension(oldName) + "_"+ suffix;
            String extn = Path.GetExtension(oldName);

            return Path.Combine(path, name+extn);
        }


    }
}
