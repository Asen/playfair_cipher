﻿namespace Playfair_Chipher
{
    partial class Main
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Main));
            this.openfileBtn = new System.Windows.Forms.Button();
            this.fileTextbox = new System.Windows.Forms.TextBox();
            this.fileLabel = new System.Windows.Forms.Label();
            this.cipherFileDialog = new System.Windows.Forms.OpenFileDialog();
            this.keyTextbox = new System.Windows.Forms.TextBox();
            this.keyLabel = new System.Windows.Forms.Label();
            this.encryptBtn = new System.Windows.Forms.Button();
            this.decryptBtn = new System.Windows.Forms.Button();
            this.keyHintLabel = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // openfileBtn
            // 
            resources.ApplyResources(this.openfileBtn, "openfileBtn");
            this.openfileBtn.Name = "openfileBtn";
            this.openfileBtn.UseVisualStyleBackColor = true;
            this.openfileBtn.Click += new System.EventHandler(this.openfileBtn_Click);
            // 
            // fileTextbox
            // 
            resources.ApplyResources(this.fileTextbox, "fileTextbox");
            this.fileTextbox.ForeColor = System.Drawing.Color.Navy;
            this.fileTextbox.Name = "fileTextbox";
            this.fileTextbox.ReadOnly = true;
            // 
            // fileLabel
            // 
            resources.ApplyResources(this.fileLabel, "fileLabel");
            this.fileLabel.Name = "fileLabel";
            // 
            // cipherFileDialog
            // 
            this.cipherFileDialog.FileName = "openFileDialog1";
            // 
            // keyTextbox
            // 
            resources.ApplyResources(this.keyTextbox, "keyTextbox");
            this.keyTextbox.Name = "keyTextbox";
            this.keyTextbox.UseSystemPasswordChar = true;
            this.keyTextbox.TextChanged += new System.EventHandler(this.keyTextbox_TextChanged);
            // 
            // keyLabel
            // 
            resources.ApplyResources(this.keyLabel, "keyLabel");
            this.keyLabel.Name = "keyLabel";
            // 
            // encryptBtn
            // 
            resources.ApplyResources(this.encryptBtn, "encryptBtn");
            this.encryptBtn.Name = "encryptBtn";
            this.encryptBtn.UseVisualStyleBackColor = true;
            this.encryptBtn.Click += new System.EventHandler(this.encryptBtn_Click);
            // 
            // decryptBtn
            // 
            resources.ApplyResources(this.decryptBtn, "decryptBtn");
            this.decryptBtn.Name = "decryptBtn";
            this.decryptBtn.UseVisualStyleBackColor = true;
            this.decryptBtn.Click += new System.EventHandler(this.decryptBtn_Click);
            // 
            // keyHintLabel
            // 
            resources.ApplyResources(this.keyHintLabel, "keyHintLabel");
            this.keyHintLabel.Name = "keyHintLabel";
            // 
            // Main
            // 
            resources.ApplyResources(this, "$this");
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.keyHintLabel);
            this.Controls.Add(this.decryptBtn);
            this.Controls.Add(this.encryptBtn);
            this.Controls.Add(this.keyLabel);
            this.Controls.Add(this.keyTextbox);
            this.Controls.Add(this.openfileBtn);
            this.Controls.Add(this.fileTextbox);
            this.Controls.Add(this.fileLabel);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow;
            this.Name = "Main";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button openfileBtn;
        private System.Windows.Forms.TextBox fileTextbox;
        private System.Windows.Forms.Label fileLabel;
        private System.Windows.Forms.OpenFileDialog cipherFileDialog;
        private System.Windows.Forms.TextBox keyTextbox;
        private System.Windows.Forms.Label keyLabel;
        private System.Windows.Forms.Button encryptBtn;
        private System.Windows.Forms.Button decryptBtn;
        private System.Windows.Forms.Label keyHintLabel;


    }
}

