﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Security;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;

namespace Playfair_Chipher
{
    class Playfair
    {

        // Размер таблицы Плейфера = 16*16 = 256 байт
        private const int TBL_SZ = 16;

        public Playfair()
        {
 
        }

        public byte[] Encrypt(byte[] bytes, String password)
        {
            // Построение таблицы по ключу
            byte[,] table = BuildPlayfairTable(GenaratePwdHash(password));
            // Построение таблицы быстрого доступа к элементам
            SortedDictionary<byte, Point> accessTable = BuildAccessTable(table);


            List<byte> cipher = new List<byte>();


            for (int i = 0; i < bytes.Length - 1; i+=2)
            {
                // Выбор биграммы
                byte b1 = bytes[i];
                byte b2 = bytes[i + 1];
                Point p1; accessTable.TryGetValue(b1, out p1);
                Point p2; accessTable.TryGetValue(b2, out p2);

                // Если элементы биграммы одинаковые, то игнорируем 1 правило
                // (http://bit.ly/1TnxhAb)
                if (b1 != b2)
                {
                    // Втророе правило Плейфера
                    if (p1.Y == p2.Y)
                    {
                        p1.X = p1.X == TBL_SZ ? 0 : p1.X++;
                        p2.X = p2.X == TBL_SZ ? 0 : p2.X++;
                    }
                    // Третье правило Плейфера
                    else if (p1.X == p2.X)
                    {
                        p1.Y = p1.Y == TBL_SZ ? 0 : p1.Y++;
                        p2.Y = p2.Y == TBL_SZ ? 0 : p2.Y++;
                    }
                    // Четвертое правило Плейфера
                    else
                    {
                        int tmpX = p1.X;
                        p1.X = p2.X;
                        p2.X = tmpX;
                    }
                }

                // Добавление к шифротексту новой биграммы
                cipher.Add(table[p1.Y, p1.X]);
                cipher.Add(table[p2.Y, p2.X]);

            }

            // В случае, если кол-во исходных байт нечетно,
            // последний не шифруем(исходя из игнорирования 1 правила Плейфера)
            if (bytes.Length % 2 == 1)
                cipher.Add(bytes[bytes.Length - 1]);


            return cipher.ToArray();
        }



        public byte[] Decrypt(byte[] bytes, String password)
        {
            // Построение таблицы по ключу
            byte[,] table = BuildPlayfairTable(GenaratePwdHash(password));
            // Построение таблицы быстрого доступа к элементам
            SortedDictionary<byte, Point> accessTable = BuildAccessTable(table);


            List<byte> decipher = new List<byte>();


            for (int i = 0; i < bytes.Length - 1; i += 2)
            {
                // Выбор биграммы из шифротекста
                byte b1 = bytes[i];
                byte b2 = bytes[i + 1];
                Point p1; accessTable.TryGetValue(b1, out p1);
                Point p2; accessTable.TryGetValue(b2, out p2);

                // Если элементы биграммы одинаковые, то игнорируем инверсию 1 правила
                // (http://bit.ly/1TnxhAb)
                if (b1 != b2)
                {
                    // Инверсия втророго правило Плейфера
                    if (p1.Y == p2.Y)
                    {
                        p1.X = p1.X == 0 ? 0 : p1.X--;
                        p2.X = p2.X == 0 ? 0 : p2.X--;
                    }
                    // Инверсия третьего правила Плейфера
                    else if (p1.X == p2.X)
                    {
                        p1.Y = p1.Y == 0 ? 0 : p1.Y--;
                        p2.Y = p2.Y == 0 ? 0 : p2.Y--;
                    }
                    // Инверсия четвертого правила Плейфера
                    else
                    {
                        int tmpX = p1.X;
                        p1.X = p2.X;
                        p2.X = tmpX;
                    }
                }

                // Добавление к шифротексту оригинальной биграммы
                decipher.Add(table[p1.Y, p1.X]);
                decipher.Add(table[p2.Y, p2.X]);

            }

            // В случае, если кол-во исходных байт нечетно,
            // восстанавливаем последний байт "как есть"
            // (исходя из игнорирования 1 правила Плейфера)
            if (bytes.Length % 2 == 1)
                decipher.Add(bytes[bytes.Length - 1]);


            return decipher.ToArray();
        }



        // Генерация хэша на основе пароля
        private byte[] GenaratePwdHash(String password)
        {
            // Усиление пароля для генерации наиболее дифферинцированного хэша
            StringBuilder strengthenPwd = new StringBuilder();
            int round = 0;
            while (strengthenPwd.Length < 4096)
            {
                // ...Используя последовательный XOR со сдвигом:
                for (int i = 0; i < password.Length; ++i )
                    strengthenPwd.Append((char)(password[i] ^ ((round*i) % 255)));

                round++;
            }

            // Вычисление хэша
            string primaryHash = strengthenPwd.ToString();
            byte[] pwd_bytes = Encoding.Unicode.GetBytes(primaryHash.ToCharArray());
       
            // (В хэш попадают только оригинальные байты)
            HashSet<byte> sequence = new HashSet<byte>();
            foreach (byte b in pwd_bytes)
                sequence.Add(b);

            return sequence.ToArray();
        }



        // Построение таблицы Плейфера на основе вычисленного хэша пароля
        private byte[,] BuildPlayfairTable(byte[] pwdhash)
        {
            // Пул уже добавленных в таблицу значений
            List<byte> bytesPool = new List<byte>(pwdhash);

            byte[,] table = new byte[TBL_SZ,TBL_SZ];
            int iteration = 0; // номер итерации(0..255)
            int hash_len = bytesPool.Count;
            byte next_byte = 0x0; // следующее "выпавшее" значение

            for(int y=0; y < TBL_SZ; ++y)
                for (int x = 0; x < TBL_SZ; ++x)
                {
                    // Заполнение таблицы сначала значениями из хэша
                    if (iteration < hash_len)
                        table[y, x] = bytesPool[iteration++];
                    // ...а затем остальными "выпавшими" значениями
                    else
                    {
                        // Поиск следующего "выпавшего" значения
                        while (bytesPool.Contains(next_byte))
                            next_byte++;

                        table[y, x] = next_byte;
                        // Добавление в пул
                        bytesPool.Add(next_byte);
                    }
                }

            return table;
        }



        // Генерация таблицы быстрого доступа к данным таблицы Плейфера
        /*
         *  Это нужно для того, чтобы крипто-процесс не замедлялся в 256 раз:
         *  если есть, например, файл в 10мб, 
         *  то сложность его шифрования = 10*1024*1024*(256) = ~2.7млрд итераций
         
         * Таблица доступа позволит не производить поиск значения в таблице Плейфера
         * каждый раз, а обращаться за константное время, таким образом
         * крипто-процесс для файла в 10мб = 10*1024*1024 = 10485760 итераций
         * 
         */
        private SortedDictionary<byte, Point> BuildAccessTable(byte[,] playfairTable)
        {
            // Ключом выступает значение из таблицы Плейфера, 
            // а значением - координата в таблице Плейфера  
            SortedDictionary<byte, Point> accessTable = new SortedDictionary<byte, Point>();

            for(int y=0; y < TBL_SZ; ++y)
                for (int x = 0; x < TBL_SZ; ++x)
                    accessTable.Add(playfairTable[y, x], new Point(x, y));

            return accessTable;
        }



    }
}
